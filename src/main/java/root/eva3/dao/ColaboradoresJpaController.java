/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva3.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.eva3.dao.exceptions.NonexistentEntityException;
import root.eva3.dao.exceptions.PreexistingEntityException;
import root.eva3.entity.Colaboradores;

/**
 *
 * @author axeld
 */
public class ColaboradoresJpaController implements Serializable {

    public ColaboradoresJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit"); 

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Colaboradores colaboradores) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(colaboradores);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findColaboradores(colaboradores.getRut()) != null) {
                throw new PreexistingEntityException("Colaboradores " + colaboradores + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Colaboradores colaboradores) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            colaboradores = em.merge(colaboradores);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = colaboradores.getRut();
                if (findColaboradores(id) == null) {
                    throw new NonexistentEntityException("The colaboradores with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Colaboradores colaboradores;
            try {
                colaboradores = em.getReference(Colaboradores.class, id);
                colaboradores.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The colaboradores with id " + id + " no longer exists.", enfe);
            }
            em.remove(colaboradores);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Colaboradores> findColaboradoresEntities() {
        return findColaboradoresEntities(true, -1, -1);
    }

    public List<Colaboradores> findColaboradoresEntities(int maxResults, int firstResult) {
        return findColaboradoresEntities(false, maxResults, firstResult);
    }

    private List<Colaboradores> findColaboradoresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Colaboradores.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Colaboradores findColaboradores(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Colaboradores.class, id);
        } finally {
            em.close();
        }
    }

    public int getColaboradoresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Colaboradores> rt = cq.from(Colaboradores.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
