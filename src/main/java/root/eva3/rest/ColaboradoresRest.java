/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva3.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.eva3.dao.ColaboradoresJpaController;
import root.eva3.dao.exceptions.NonexistentEntityException;
import root.eva3.entity.Colaboradores;

/**
 *
 * @author axeld
 */
@Path("colaborador")
public class ColaboradoresRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listaColaboradores() {
        ColaboradoresJpaController dao = new ColaboradoresJpaController();

        List<Colaboradores> lista = dao.findColaboradoresEntities();
        return Response.ok(200).entity(lista).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Colaboradores colaborador) {

        ColaboradoresJpaController dao = new ColaboradoresJpaController();

        try {
            dao.create(colaborador);
        } catch (Exception ex) {
            Logger.getLogger(ColaboradoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(colaborador).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Colaboradores colaborador) {
        ColaboradoresJpaController dao = new ColaboradoresJpaController();

        try {
            dao.edit(colaborador);
        } catch (Exception ex) {
            Logger.getLogger(ColaboradoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(colaborador).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")
    public Response eliminar(@PathParam("rut") String rut) {

        try {
            ColaboradoresJpaController dao = new ColaboradoresJpaController();

            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(ColaboradoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok("Colaborador eliminado").build();
    }

}
