/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva3.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author axeld
 */
@Entity
@Table(name = "colaboradores")
@NamedQueries({
    @NamedQuery(name = "Colaboradores.findAll", query = "SELECT c FROM Colaboradores c"),
    @NamedQuery(name = "Colaboradores.findByRut", query = "SELECT c FROM Colaboradores c WHERE c.rut = :rut"),
    @NamedQuery(name = "Colaboradores.findByNombres", query = "SELECT c FROM Colaboradores c WHERE c.nombres = :nombres"),
    @NamedQuery(name = "Colaboradores.findByApellidos", query = "SELECT c FROM Colaboradores c WHERE c.apellidos = :apellidos"),
    @NamedQuery(name = "Colaboradores.findByUnidad", query = "SELECT c FROM Colaboradores c WHERE c.unidad = :unidad"),
    @NamedQuery(name = "Colaboradores.findBySueldo", query = "SELECT c FROM Colaboradores c WHERE c.sueldo = :sueldo")})
public class Colaboradores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombres")
    private String nombres;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "apellidos")
    private String apellidos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "unidad")
    private String unidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "sueldo")
    private String sueldo;

    public Colaboradores() {
    }

    public Colaboradores(String rut) {
        this.rut = rut;
    }

    public Colaboradores(String rut, String nombres, String apellidos, String unidad, String sueldo) {
        this.rut = rut;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.unidad = unidad;
        this.sueldo = sueldo;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }

    public String getSueldo() {
        return sueldo;
    }

    public void setSueldo(String sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colaboradores)) {
            return false;
        }
        Colaboradores other = (Colaboradores) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.eva3.entity.Colaboradores[ rut=" + rut + " ]";
    }
    
}
