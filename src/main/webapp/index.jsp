<%-- 
    Document   : index
    Created on : 19-10-2021, 13:11:09
    Author     : axeld
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Autor: Axel Donoso Sánchez</h1>
        <h1>Sección: 51</h1>
        <p>Listar colaboradores GET http://eva3axeldonoso.herokuapp.com/api/colaborador</p>
        <p>Crear colaboradores POST http://eva3axeldonoso.herokuapp.com/api/colaborador</p>
        <p>Modificar colaboradores GET http://eva3axeldonoso.herokuapp.com/api/colaborador</p>
        <p>Eliminar colaboradores GET http://eva3axeldonoso.herokuapp.com/api/colaborador</p>
    </body>
</html>
